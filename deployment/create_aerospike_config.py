import subprocess
import parse

AEROSPIKE_HOSTS = [f"st117vm10{i}" for i in [2, 3, 4, 5, 6, 7, 8, 9]]

config_head = """
service {
	proto-fd-max 15000
}

logging {
    file /var/log/aerospike/aerospike.log {
        context any info
    }
}

network {
	service {
		address any
		access-address"""
config_tail = """

		interval 150
		timeout 10
	}

	fabric {
		port 3001
	}

	info {
		port 3003
	}
}

mod-lua {
    user-path /opt/aerospike/usr/udf/lua
}

namespace allezon {
	replication-factor 1
	memory-size 3G

	storage-engine device {
		file /opt/aerospike/data/allezon.dat
		filesize 25G
	}
}
"""

nodes = AEROSPIKE_HOSTS
hostname = subprocess.run(['hostname'], capture_output=True).stdout.strip().decode("utf-8")
my_ip = subprocess.run(['ip', '-f', 'inet', 'addr', 'show', 'eth0'], capture_output=True).stdout.strip().decode("utf-8")
my_ip = parse.parse("{} inet {ip}/{}", my_ip)['ip']

ips = []
for node in nodes:
    if hostname == node:
        continue
    ips.append(subprocess.run(['dig', '+short', f'{node}.rtb-lab.pl'], capture_output=True).stdout.strip().decode("utf-8"))

config = config_head + f" {my_ip}\n		    port 3000\n" + "    }\n\n	heartbeat {\n"
config += f"""
		mode mesh
		address {my_ip}
		port 3002
		
		mesh-seed-address-port {my_ip} 3002
"""

for ip in ips:
    config += f"         mesh-seed-address-port {ip} 3002\n"
config += config_tail

with open("aerospike.conf", "w") as f:
    f.write(config)
