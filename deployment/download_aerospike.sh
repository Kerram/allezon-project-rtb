#!/bin/bash -v

wget -O aerospike.tgz https://download.aerospike.com/download/server/6.0.0.3/artifact/ubuntu20
tar xzvf aerospike.tgz
cd aerospike-server-community-6.0.0.3-ubuntu20.04/
sudo ./asinstall
