#!/bin/bash -v

# Wildcard privileges thanks to https://stackoverflow.com/questions/19101243/error-1130-hy000-host-is-not-allowed-to-connect-to-this-mysql-server.
echo "CREATE DATABASE allezon; USE allezon; CREATE TABLE tags(
  time TIMESTAMP(3) NOT NULL,
  action CHAR(4) NOT NULL,
  origin VARCHAR(30) NOT NULL,
  brand_id VARCHAR(100) NOT NULL,
  category_id VARCHAR(100) NOT NULL,
  price INT NOT NULL,
  PRIMARY KEY (time)
) ENGINE=INNODB; CREATE USER 'user'@'%' IDENTIFIED BY 'pass'; GRANT ALL PRIVILEGES ON *.* TO 'user'@'%'; FLUSH PRIVILEGES;" | mysql -u root -p

