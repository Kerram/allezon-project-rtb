#!/bin/bash -v

USERNAME=$1
PASSWORD=$2

sshpass -p ${PASSWORD} ssh ${USERNAME}vm110.rtb-lab.pl 'git clone https://gitlab.com/Kerram/allezon-project-rtb.git'
./install_db_local.sh 01

echo "SSH into VM10 and run install_db_local.sh 10 and start_db.sh!"
