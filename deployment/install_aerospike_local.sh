#!/bin/bash -v

sudo apt -y install python3-venv
python3 -m venv ./venv

source ./venv/bin/activate
pip install -r requirements.txt
python3 ./create_aerospike_config.py

sudo mkdir /var/log/aerospike
sudo cp aerospike.conf /etc/aerospike/

