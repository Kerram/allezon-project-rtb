## How to start Allezon service

Due to the hardcoding of some ip addresses, you need to run the given commands on correct VMs.

The following steps will set up Docker swarm, Aerospike and MySQL database together with our service (running in the swarm).
Steps:
- Reset all VMs
- [On st117vm101] Clone public repository https://gitlab.com/Kerram/allezon-project-rtb.git
- [On st117vm101] Run `install.sh st117 {password for st117}` from folder **deployment**
- [On st117vm101] Run `start_services.sh st117 {password for st117}` from folder **deployment**
- **[On st117vm110]** Run `install_db_local.sh 10` and `start_db.sh!` from folder **deployment**
- **[On st117vm101]** Run `run_service` from folder **deployment**

**WATCH OUT!**: Due to some permission problems you need to make sure (before running service - last step) that file `/etc/mysql/my.cnf`
on both **st117vm101** and **st117vm110** have section (NODE_ID is either 01 or 10):
```
[mysqld]
skip-log-bin
bind-address=10.112.117.1{NODE_ID}
```
After modifying such files run `sudo systemctl restart mysql.service` on both machines for the changes to take effect.

## Architecture overview

Service logic is written in Python in FastAPI framework. It is scaled to 10 instances via Docker Swarm tool.
This tool already takes care of load balancing, so this is not part of the application logic.

After receiving a user tag, we store it in Aerospike hosted on 8 instances (st117vm1[02:09]).
The aerospike has replication factor set to 1 to preserve memory on disk (it does not store data in memory, because we don't
have enough RAM on our machines). For more details see section about Aerospike.

In addition to putting a tag in Aerospike, we also store part of it in regular RDBMS. Our database of choice is MySQL.
We host two instances/shards (on st117vm101 and st117vm110). We store these parts of tags that are related to the aggregate queries.
The data is sharded based on time (it is unique, so it also serves as a primary key). First database stores data
with time field before noon and the second one stores the rest. Sharding is implemented in the application logic.
For more details see section about MySQL.

## FastAPI application

FastAPI application (responsible for business logic) is run inside a docker container and scaled to 10 replicas via Docker Swarm.
Docker images are hoster in Gitlab Container Registry linked to the project's
Git repository: https://gitlab.com/Kerram/allezon-project-rtb/container_registry.

## Aerospike

Adding new tag to Aerospike (key-value store) is done in a synchronous way.
This means, that we return response only after we add new entry to Aerospike.

We use cookies as keys and lists of dicts as values. We couldn't figure out a way to keep the lists sorted using Aerospike
API, to the lists are unordered. Despite adding tags in a chronological order, we don't have a guarantee that the lists will be sorted by time,
because of the distributed character of the system (we don't lock any records in Aerospike explicitly).
To fight this, we raised the limit of lists' lengths to 220 and treat them as sorted.
This heuristic proved to be quite fast and reliable.

## MySQL

Adding new tag to MySQL database is done in an asynchronous way.
We don't use any external event queue. Instead, after receiving a new tag, we put it into a thread-safe Python queue shared
among the threads of FastAPI application. After placing the tag in the queue we add it to Aerospike (so Aerospike errors do
not affect tag insertion to DB) and then return response code 204.
As a side effect, the user will not know whether the tag was inserted correctly from the response.

When execution gets to the function responsible for actually inserting tags into the DB, we add them in batches.
All interaction with the database is done via pure/unsafe SQL, we don't use ORMs or any validation.

## Data retention

We keep the lengths of Aerospike lists below 221 entries. This is done immediately after insertion of new tag, even before
returning 204 response. We just pop the end of the list hoping that slack of 20 more entries (above the required limit of 200)
will mitigate the impact of distributed system on the tags ordering.

Every 3 minutes (more or less) we run a DELETE query on both shards deleting tags older than 24h since the last tag seen by
the system.

## Known bugs and issues

- It appears that sharding logic has some minor bug, because when we cross the noon point in incoming data, we get wrong answers.
After a while everything goes back to normal. However, when tested on small examples sharding logic worked correctly, so the problem can
lie elsewhere.
- User tags and user profiles endpoints are getting some timeouts.
- Because thread-safe Python queue is synchronous and asynchronous Python queue is not thread-safe, we make use of active waiting "technique" :(.

## Sources

- Swarm, docker and ansible configs/scripts are based on https://mimuw.rtbhouse.com/slides/1.pdf.
- Aerospike setup is based on https://github.com/RTBHOUSE/mimuw-lab/tree/main/lab07.