import logging
from datetime import datetime, timedelta
from logging.config import dictConfig
from typing import List

from src.configs import log_config, MORNING_ENDS_HOUR
from src.models import UserTag, AggregateType

INSERT_TAG_QUERY = "INSERT INTO tags (time, action, origin, brand_id, category_id, price) VALUES "
DELETE_OLD_TAGS_QUERY = "DELETE from tags WHERE time < %s"
AGGREGATE_TAG_QUERY_1 = "FROM tags WHERE time >= '{begin}' and time < '{end}' "
AGGREGATE_TAG_QUERY_2 = "ORDER BY time"
FILTER_COLUMNS = ["action", "origin", "brand_id", "category_id"]

dictConfig(log_config)
logger = logging.getLogger("main_logger")


def get_values_str(user_tag: UserTag):
    return f"('{user_tag.time}'," \
           f"'{user_tag.action.value}'," \
           f"'{user_tag.origin}'," \
           f"'{user_tag.product_info['brand_id']}'," \
           f"'{user_tag.product_info['category_id']}'," \
           f"'{user_tag.product_info['price']}')"


async def insert_tags_with_cursor(cursor, user_tags: List[UserTag]):
    query = INSERT_TAG_QUERY + get_values_str(user_tags[0])

    for user_tag in user_tags[1:]:
        query += ", " + get_values_str(user_tag)

    await cursor.execute(query)


async def insert_tags(morning_connection, evening_connection, user_tags: List[UserTag]):
    morning_tags = list(filter(lambda tag: tag.time.hour < MORNING_ENDS_HOUR, user_tags))
    evening_tags = list(filter(lambda tag: tag.time.hour >= MORNING_ENDS_HOUR, user_tags))

    if len(morning_tags) > 0:
        async with morning_connection.cursor() as cursor:
            await insert_tags_with_cursor(cursor, morning_tags)
            await morning_connection.commit()

    if len(evening_tags) > 0:
        async with evening_connection.cursor() as cursor:
            await insert_tags_with_cursor(cursor, evening_tags)
            await evening_connection.commit()


def gen_aggregate_query_string(begin, end, filters, aggregates):
    query = "SELECT time"
    for column in FILTER_COLUMNS:
        if column in filters.keys():
            query += f", {column}"

    for aggre in aggregates:
        if aggre == AggregateType.COUNT:
            query += ", count(*)"
        elif aggre == AggregateType.SUM_PRICE:
            query += ", sum(price)"
    query += " "

    query += AGGREGATE_TAG_QUERY_1.format(begin=begin, end=end)
    for (column, value) in filters.items():
        query += f"AND {column} = '{value}' "
    query += "GROUP BY time"

    for column in filters:
        query += f", {column}"
    query += " " + AGGREGATE_TAG_QUERY_2

    return query


def new_row(begin, filters, aggregates):
    row = [begin.strftime("%Y-%m-%dT%H:%M:%S")]
    for column in FILTER_COLUMNS:
        if column in filters:
            row.append(filters[column])

    for _ in aggregates:
        row.append(0)

    return row


async def aggregate_query(mysql_connection, begin, end, filters, aggregates):
    if begin >= end:
        return []

    query = gen_aggregate_query_string(begin, end, filters, aggregates)
    async with mysql_connection.cursor() as cursor:
        await cursor.execute(query)

        rows = [new_row(begin, filters, aggregates)]
        for row in await cursor.fetchall():
            while row[0] - begin >= timedelta(minutes=1):
                begin += timedelta(minutes=1)
                rows.append(new_row(begin, filters, aggregates))

            for i in range(len(aggregates)):
                rows[-1][-(i + 1)] = rows[-1][-(i + 1)] + row[-(i + 1)]

        while end - begin > timedelta(minutes=1):
            begin += timedelta(minutes=1)
            rows.append(new_row(begin, filters, aggregates))
        return rows


async def delete_old_tags(mysql_connection, tag):
    tag_time = tag.time
    forgettable_past = datetime(tag_time.year, tag_time.month, tag_time.day, tag_time.hour, tag_time.minute, 0) - \
                       timedelta(hours=24)
    async with mysql_connection.cursor() as cursor:
        await cursor.execute(DELETE_OLD_TAGS_QUERY, (forgettable_past,))
        await mysql_connection.commit()
