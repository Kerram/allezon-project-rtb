from datetime import datetime
from enum import Enum
from typing_extensions import TypedDict
from pydantic import BaseModel, Extra


class DeviceType(Enum):
    PC = "PC"
    MOBILE = "MOBILE"
    TV = "TV"


class ActionType(Enum):
    VIEW = "VIEW"
    BUY = "BUY"


class AggregateType(Enum):
    COUNT = "COUNT"
    SUM_PRICE = "SUM_PRICE"


class ProductInfo(TypedDict):
    product_id: str
    brand_id: str
    category_id: str
    price: int


class UserTag(BaseModel):
    time: datetime
    cookie: str
    country: str
    device: DeviceType
    action: ActionType
    origin: str
    product_info: ProductInfo

    class Config:
        extra = Extra.forbid
