import asyncio
from datetime import datetime
from queue import Queue, Empty

import aiomysql
from fastapi import FastAPI, Response, Request, Query
import socket
import aerospike
import pytz
import logging
from aerospike_helpers.operations import list_operations
from logging.config import dictConfig

from src.configs import AEROSPIKE_CONFIG, AEROSPIKE_PROFILES, AEROSPIKE_NAMESPACE, USER_TAG_LIMIT, log_config, \
    BATCH_INSERT_TAG_DB_LIMIT, MAX_INSERT_RETRIES, DELETE_OLD_TAGS_TIME_PERIOD, \
    MYSQL_ASYNC_CONFIG_MORNING, MYSQL_ASYNC_CONFIG_EVENING, MORNING_ENDS_HOUR
from src.models import UserTag, ActionType, AggregateType
from src.sql_queries import insert_tags, aggregate_query, delete_old_tags

dictConfig(log_config)
logger = logging.getLogger("main_logger")
app = FastAPI()
aerospike_client = None
server_running = False
tags_queue = None
mysql_pool_morning = None
mysql_pool_evening = None
last_tag_inserted_into_db = None


async def insert_tags_daemon(morning_pool, evening_pool):
    global last_tag_inserted_into_db

    morning_connection = await morning_pool.acquire()
    evening_connection = await evening_pool.acquire()
    while server_running:
        await asyncio.sleep(0)
        tags = []

        while len(tags) < BATCH_INSERT_TAG_DB_LIMIT:
            try:
                tags.append(tags_queue.get(block=False))
            except Empty:
                break

        if len(tags) == 0:
            continue

        success = False
        retries = MAX_INSERT_RETRIES
        while server_running and not success and retries > 0:
            try:
                await insert_tags(morning_connection, evening_connection, tags)
                success = True
            except Exception as ex:
                logger.error(f"Exception in tag insertion into DB: {ex}.")
                logger.info("Trying again with new MYSQL connection...")
                morning_connection.close()
                evening_connection.close()
                morning_pool.release(morning_connection)
                evening_pool.release(evening_connection)
                morning_connection = await morning_pool.acquire()
                evening_connection = await evening_pool.acquire()
                retries -= 1
        last_tag_inserted_into_db = tags[-1]

    morning_pool.release(morning_connection)
    evening_pool.release(evening_connection)

    morning_pool.close()
    await morning_pool.wait_closed()

    evening_pool.close()
    await evening_pool.wait_closed()


def insert_aerospike_tag(aerospike, user_tag):
    try:
        key = (AEROSPIKE_NAMESPACE, AEROSPIKE_PROFILES, user_tag.cookie)

        if user_tag.action == ActionType.VIEW:
            bin_name = 'views'
        elif user_tag.action == ActionType.BUY:
            bin_name = 'buys'
        else:
            raise ValueError("Not recognizable action type!")

        op = list_operations.list_append(bin_name, user_tag.dict())
        size_op = list_operations.list_size(bin_name)

        (_, _, res) = aerospike.operate(key, [op, size_op])

        if res[bin_name] > USER_TAG_LIMIT:
            op = list_operations.list_pop(bin_name, 0)
            aerospike.operate(key, [op])
    except Exception as ex:
        logger.warning(f"Exception {ex} in user tags.")
        raise ex


async def delete_old_tags_in_db(pools):
    while server_running:
        await asyncio.sleep(DELETE_OLD_TAGS_TIME_PERIOD)
        for pool in pools:
            async with pool.acquire() as connection:
                await delete_old_tags(connection, last_tag_inserted_into_db)
                logger.info(f"Deleting old tags with newest tag time {last_tag_inserted_into_db.time}")


@app.on_event("startup")
async def setup():
    global aerospike_client, tags_queue, server_running, mysql_pool_morning, mysql_pool_evening

    tags_queue = Queue()
    server_running = True

    try:
        aerospike_client = aerospike.client(AEROSPIKE_CONFIG).connect()
    except Exception as ex:
        raise RuntimeError(f"Failed to connect to the cluster with {AEROSPIKE_CONFIG['hosts']}. Exception cause: {ex}.")
    logger.info("Aerospike connected successfully")

    mysql_pool_morning = await aiomysql.create_pool(**MYSQL_ASYNC_CONFIG_MORNING, loop=asyncio.get_event_loop())
    logger.info("Connected to morning DB")

    mysql_pool_evening = await aiomysql.create_pool(**MYSQL_ASYNC_CONFIG_EVENING, loop=asyncio.get_event_loop())
    logger.info("Connected to evening DB")

    asyncio.create_task(insert_tags_daemon(mysql_pool_morning, mysql_pool_evening))
    asyncio.create_task(delete_old_tags_in_db([mysql_pool_morning, mysql_pool_evening]))

    logger.info("Setup completed")


@app.on_event("shutdown")
def teardown():
    global server_running

    server_running = False
    if aerospike_client is not None:
        aerospike_client.close()


@app.get("/")
def root():
    return {"hostname": socket.gethostname()}


@app.post("/user_tags")
def user_tags(user_tag: UserTag):
    tags_queue.put(user_tag)
    insert_aerospike_tag(aerospike_client, user_tag)

    return Response(status_code=204)


def filter_sort_time_range(tags, time_range):
    format_string = "%Y-%m-%dT%H:%M:%S.%f"

    begin = time_range.split("_")[0] + "000"
    end = time_range.split("_")[1] + "000"

    begin = pytz.UTC.localize(datetime.strptime(begin, format_string))
    end = pytz.UTC.localize(datetime.strptime(end, format_string))

    res = list(filter(lambda tag: begin <= tag['time'] < end, tags))
    res.sort(key=lambda tag: tag['time'])

    return list(reversed(res))


@app.post("/user_profiles/{cookie}")
def user_profiles(cookie: str, time_range: str, limit: int = 200):
    (_, _, record) = aerospike_client.get((AEROSPIKE_NAMESPACE, AEROSPIKE_PROFILES, cookie))

    views = []
    if 'views' in record:
        views = record['views']

    buys = []
    if 'buys' in record:
        buys = record['buys']

    buys = filter_sort_time_range(buys, time_range)
    views = filter_sort_time_range(views, time_range)

    response = {
        "cookie": cookie,
        "views": views[:limit],
        "buys": buys[:limit],
    }

    return response


def parse_time_1m_range(time_range):
    format_string = "%Y-%m-%dT%H:%M:%S"

    begin = time_range.split("_")[0]
    end = time_range.split("_")[1]

    begin = datetime.strptime(begin, format_string)
    end = datetime.strptime(end, format_string)

    return begin, end


@app.post("/aggregates")
async def aggregates_endpoint(time_range: str,
                        action: ActionType,
                        aggregates: list[AggregateType] = Query(),
                        origin: str = None,
                        brand_id: str = None,
                        category_id: str = None):

    (begin, end) = parse_time_1m_range(time_range)

    filters = {'action': action.value}
    if origin is not None:
        filters['origin'] = origin
    if brand_id is not None:
        filters['brand_id'] = brand_id
    if category_id is not None:
        filters['category_id'] = category_id

    morning_end = end
    begin_evening = begin
    if morning_end.hour >= MORNING_ENDS_HOUR:
        morning_end = datetime(morning_end.year, morning_end.month, morning_end.day, MORNING_ENDS_HOUR, 0, 0)
    if begin_evening.hour < MORNING_ENDS_HOUR:
        begin_evening = datetime(begin_evening.year, begin_evening.month, begin_evening.day, MORNING_ENDS_HOUR, 0, 0)
    async with mysql_pool_morning.acquire() as connection:
        try:
            rows = await aggregate_query(connection, begin, morning_end, filters, aggregates)
        except Exception as ex:
            logger.warning(f"Aggregate morning query got exception: {ex}.")

    async with mysql_pool_evening.acquire() as connection:
        try:
            rows += await aggregate_query(connection, begin_evening, end, filters, aggregates)
        except Exception as ex:
            logger.warning(f"Aggregate evening query got exception: {ex}.")

    columns = ["1m_bucket"]
    for column in filters.keys():
        columns.append(column)
    for aggre in aggregates:
        columns.append(aggre.value.lower())

    for row in rows:
        for i in range(len(aggregates)):
            row[-(i + 1)] = str(row[-(i + 1)])

    result = {
        "columns": columns,
        "rows": rows,
    }

    return result
