AEROSPIKE_CONFIG = {
    'hosts': [
        ('10.112.117.102', 3000),
        ('10.112.117.103', 3000),
        ('10.112.117.104', 3000),
        ('10.112.117.105', 3000),
        ('10.112.117.106', 3000),
        ('10.112.117.107', 3000),
        ('10.112.117.108', 3000),
        ('10.112.117.109', 3000),
    ]
}

USER_TAG_LIMIT = 220  # Excess to fight race conditions
AEROSPIKE_NAMESPACE = 'allezon'
AEROSPIKE_PROFILES = 'profiles'

MYSQL_ASYNC_CONFIG_MORNING = {
    'user': 'user',
    'password': 'pass',
    'host': '10.112.117.101',
    'db': 'allezon',
    'pool_recycle': 1.0,
    'connect_timeout': 5,
}

MYSQL_ASYNC_CONFIG_EVENING = {
    'user': 'user',
    'password': 'pass',
    'host': '10.112.117.110',
    'db': 'allezon',
    'pool_recycle': 1.0,
    'connect_timeout': 5,
}

BATCH_INSERT_TAG_DB_LIMIT = 1000
MAX_INSERT_RETRIES = 2
DELETE_OLD_TAGS_TIME_PERIOD = 180
MORNING_ENDS_HOUR = 12

# Big Thanks to Yash Nag's and JPG's answers from
# https://stackoverflow.com/questions/63510041/adding-python-logging-to-fastapi-endpoints-hosted-on-docker-doesnt-display-api.
log_config = {
    "version": 1,
    "formatters": {
        "main": {
            "format": "%(levelname)s::%(asctime)s:    %(module)s  ---  %(message)s"
        },
    },
    "handlers": {
        "main_handler": {
            "class": "logging.FileHandler",
            "formatter": "main",
            "filename": "main.log",
        },
    },
    "loggers": {
        "main_logger": {
            "level": "DEBUG",
            "handlers": ["main_handler"],
        },
    },
    "disable_existing_loggers": False,
}
