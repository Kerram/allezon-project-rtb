# syntax=docker/dockerfile:1
FROM python:3.9-slim
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8000

COPY src/ src/
COPY log.ini log.ini
CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "80", "--log-config", "log.ini"]

